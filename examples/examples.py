import sys
sys.path.append("../")
from fci_calculator import FCI_Calculator

for nelec, norb, solution in [
    [2, 2, -196.28983910],
    [4, 3, -205.34643997],
    [4, 4, -205.48130836],
    [6, 6, -213.73950088],
    [6, 9, -214.79652966],
    [12, 9, -224.72515676],
]:
    fcipath = f"./fcidumps/CAS{nelec}{norb}_ozone.FciDmp"

    print(f"CAS{nelec}{norb}")

    # Set up the calculator
    calc = FCI_Calculator(fcipath=fcipath)

    # Build the Hamiltonian
    # ncore = 1 (default)

    hamil = calc.build_hamil(ncore=18)

    # Diagonalise the Hamiltonian to obtain
    # the lowest energy + eigenvector

    energy, vec = calc.diagonalize_H(hamil)

    print("{0:+.7f} {1:10s} {2:+.7f}".format(energy, "solution: ", solution))

# In order to use Löwdin Perturbation Theory
# Use the build_Loewdin() function

fcipath = "./fcidumps/CAS129_ozone.FciDmp"

calc = FCI_Calculator(fcipath=fcipath)

# Required arguments are the number of spatial orbitals
# in each GAS space, the minimum and the maximum number
# of electrons in each GAS space

GAS_sizes = [3, 4, 2]
GAS_min = [2, 6, 0]
GAS_max = [4, 8, 4]

hamil_loewdin = calc.build_loewdin(
    GAS_sizes=GAS_sizes, GAS_min=GAS_min, GAS_max=GAS_max, ncore=18
)

print("\nLöwdin perturbed result: ",energy)
