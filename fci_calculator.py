import numpy as np
import itertools
import re
import sys


class FCI_Calculator:

    def __init__(self, fcipath, basis="slater"):
        # Basis
        self.nspatorbs = 0  # Number of spatial orbitals
        self.nspinorbs = 0
        self.nelecs = 0  # Number of electrons
        # total spin projection (n(alpha) - n(beta))*0.5
        self.total_spin = 0
        self.Stotal = 0
        # FCIDUMP
        self.fcipath = fcipath
        self.fcidump = self.read_fcidump()
        self.basis = basis
        if self.basis == "slater":
            # Slater determinants
            self.occ_vecs = self.get_occ_vecs()  # occupation number vectors
        elif self.basis == "guga":
            self.csfs = []
            self.csf = []
            self.get_csfs()
            self.necipath = ""
        else:
            print(r"Invalid basis, valid options are 'slater' and 'guga'")
            sys.exit()
        # Löwdin
        self.GAS_sizes = []
        self.GAS_min = []
        self.GAS_max = []
        self.prop_vecs = []
        self.contained_in_P = []
        self.P_space = []
        self.Q_space = []

    def get_occ_vecs(self):
        self.occ_vecs = (
            np.array(
                [
                    vec
                    for vec in itertools.combinations(
                        range(self.nspinorbs), self.nelecs
                    )
                    if self.calc_spin_proj(np.array(vec)) == self.total_spin
                ],
                dtype=int,
            )
            + 1
        )
        return self.occ_vecs

    def calc_spin_proj(self, vec):
        spin = 0
        for idx in range(len(vec)):
            if vec[idx] % 2 == 0:
                spin -= 0.5
            else:
                spin += 0.5
        return spin

    def read_fcidump(self):
        reading = False
        in_header = True
        with open(self.fcipath) as file:
            header = np.array([])
            for line in file:

                if in_header:
                    header = np.concatenate((header, line.split(",")))

                if "&END" in line:
                    reading = True
                    in_header = False

                    for entry in header:
                        if "NORB" in entry:
                            val = int(re.search(r"\d+", entry).group())
                            self.nspatorbs = val
                            self.nspinorbs = 2 * val
                            self.fcidump = {
                                "onelec": np.zeros([self.nspatorbs, self.nspatorbs])
                            }
                        if "NELEC" in entry:
                            val = int(re.search(r"\d+", entry).group())
                            self.nelecs = val
                        if "MS2" in entry:
                            val = int(re.findall(r"\d+", entry)[-1])
                            self.Stotal = val / 2

                if reading:

                    vals = np.array(
                        re.findall(r"[+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?", line)
                    ).astype(np.float64)

                    if np.sum(vals[1:] == 0) == 2:
                        # -1 -> python : zeroindexing, fcidump : oneindexing
                        self.fcidump["onelec"][int(vals[1] - 1), int(vals[2] - 1)] = (
                            vals[0]
                        )
                        self.fcidump["onelec"][int(vals[2] - 1), int(vals[1] - 1)] = (
                            vals[0]
                        )
                    elif np.sum(vals[1:] == 0) == 0 and len(vals) != 0:
                        self.fcidump[(tuple(vals[1:].astype(int)))] = vals[0]
                    elif np.sum(vals[1:] == 0) == 4:
                        self.fcidump["nuc"] = vals[0]

        return self.fcidump

    def one_electron_integral(self, indices):
        indices = np.sort(indices)[::-1]
        indices = (indices + 1) // 2
        return self.fcidump["onelec"][int(indices[0] - 1), int(indices[1] - 1)]

    def antisym_two_int(self, indices):
        """
        input = [p,r,q,s]

        This function translates
        <pr||qs> a+{p}a+{r}a{s}a{q}
        <pr||qs> = <p;r|1/r|q;s> - <p;r|1/r|s;q>
                 = (pq|rs) - (ps|rq)
        """
        res = 0.0
        p, r, q, s = indices
        # (pq|rs)
        if (p + q) % 2 == 0 and (r + s) % 2 == 0:
            indices = np.array([p, q, r, s])
            indices = (indices + 1) // 2
            indices = self.reorder_2elec_idcs(indices)
            if tuple(indices) in self.fcidump.keys():
                val = self.fcidump[tuple(indices)]
                if np.abs(val) > 1e-10:
                    res += val
            else:
                res += 0
                # print(f"{indices} not found in FCIDUMP")
        # (ps|rq)

        if (p + s) % 2 == 0 and (r + q) % 2 == 0:
            indices = np.array([p, s, r, q])
            indices = (indices + 1) // 2
            indices = self.reorder_2elec_idcs(indices)
            if tuple(indices) in self.fcidump.keys():
                val = self.fcidump[tuple(indices)]
                if np.abs(val) > 1e-10:
                    res -= val
            else:
                res -= 0
        return res

    def reorder_2elec_idcs(self, indices):

        if indices[0] < indices[1]:
            indices_tmp = np.copy(indices)
            indices[0], indices[1] = indices_tmp[1], indices_tmp[0]

        if indices[2] < indices[3]:
            indices_tmp = np.copy(indices)
            indices[2], indices[3] = indices_tmp[3], indices_tmp[2]

        if indices[2] > indices[0]:
            indices_tmp = np.copy(indices)
            indices[:2], indices[2:] = indices_tmp[2:], indices_tmp[:2]

        if indices[2] == indices[0] and indices[3] > indices[1]:
            indices_tmp = np.copy(indices)
            indices[:2], indices[2:] = indices_tmp[2:], indices_tmp[:2]

        return indices

    def differ_by(self, vec1, vec2):

        shared = np.intersect1d(vec1, vec2)

        return (len(vec1) + len(vec2) - len(shared) * 2) // 2

    def differ_where(self, vec1, vec2):

        shared = np.intersect1d(vec1, vec2)

        # There are no shared indices
        if len(shared) == 0:
            vec1_diff = np.array(range(len(vec1)))
            vec2_diff = np.array(range(len(vec2)))
        else:
            vec1_diff = np.sort(self.get_diff(vec1, shared))
            vec2_diff = np.sort(self.get_diff(vec2, shared))[::-1]

        return vec1_diff, vec2_diff

    def get_diff(self, vec, shared):

        diff = np.array([], dtype=int)
        for idx in range(len(vec)):
            if vec[idx] not in shared:
                diff = np.append(diff, np.where(vec == vec[idx])[0])

        return np.array(diff)

    def hamil_diff_one(self, vec1, vec2, diff_idx_1, diff_idx_2):
        """
        single excitations are
        sum_pq ( <p|q> + sum_(i) <pi||qi>)a+{p}a{q}
        """

        res = 0

        distance = np.abs(diff_idx_1[0] - diff_idx_2[0])

        if distance % 2 == 0:
            sign = 1
        else:
            sign = -1

        # Check if the integral is <alpha|alpha> or <beta|beta>
        if (vec1[diff_idx_1] + vec2[diff_idx_2]) % 2 == 0:
            indices = np.concatenate((vec1[diff_idx_1], vec2[diff_idx_2]))
            res += sign * self.one_electron_integral(indices)

        for idx1 in range(len(vec1)):
            for idx2 in range(len(vec2)):
                if (
                    vec1[idx1] != vec1[diff_idx_1]
                    and vec2[idx2] != vec2[diff_idx_2]
                    and vec1[idx1] == vec2[idx2]
                ):

                    idcs_1 = [diff_idx_1[0], idx1]
                    idcs_2 = [idx2, diff_idx_2[0]]

                    indices = np.concatenate((vec1[idcs_1], vec2[idcs_2[::-1]]))

                    res += sign * self.antisym_two_int(indices)
        return res

    def hamil_diff_two(self, vec1, vec2, diff_idcs_1, diff_idcs_2):
        """
        double excitations are
        sum_pqrs <pr||qs>a+{p}a+{r}a+{s}a+{q}
        """

        sign = 1

        if (diff_idcs_1[0] + diff_idcs_2[1]) % 2 != 0:
            sign *= -1

        if diff_idcs_1[0] < diff_idcs_1[1]:
            diff_tmp_1 = diff_idcs_1[1] - 1
        else:
            diff_tmp_1 = diff_idcs_1[1]

        if diff_idcs_2[1] < diff_idcs_2[0]:
            diff_tmp_2 = diff_idcs_2[0] - 1
        else:
            diff_tmp_2 = diff_idcs_2[0]

        if (diff_tmp_1 + diff_tmp_2) % 2 != 0:
            sign *= -1

        indices = np.concatenate((vec1[diff_idcs_1], vec2[diff_idcs_2[::-1]]))

        return sign * self.antisym_two_int(indices)

    def hamil_diag(self, vec):
        """
        Diagonal elements are
        sum_i <i|i> + sum_(i>j) <ij||ij>
        """

        res = 0

        # one electron part
        for idx in vec:
            indices = np.array([idx, idx])
            res += self.one_electron_integral(indices)

        # two electron part
        for idx1 in vec:
            for idx2 in vec:
                if idx1 > idx2:
                    indices = np.array([idx1, idx2, idx1, idx2])
                    res += self.antisym_two_int(indices)
        return res

    def calc_ham_entry(self, i, j):

        if i > j:
            return 0.0

        vec1 = self.occ_vecs[i]
        vec2 = self.occ_vecs[j]

        diff = self.differ_by(vec1, vec2)

        if diff > 2:
            return 0.0
        elif diff == 2:
            diff_idcs_1, diff_idcs_2 = self.differ_where(vec1, vec2)
            return self.hamil_diff_two(vec1, vec2, diff_idcs_1, diff_idcs_2)
        elif diff == 1:
            diff_idx_1, diff_idx_2 = self.differ_where(vec1, vec2)
            return self.hamil_diff_one(vec1, vec2, diff_idx_1, diff_idx_2)
        elif diff == 0 and (vec1 == vec2).all():
            return self.hamil_diag(vec1) + self.fcidump["nuc"]

    def build_hamil(self, ncore=1):

        if self.basis == "guga":
            print("Use build_guga_hamil")
            sys.exit()

        dim = len(self.occ_vecs)
        hamil = np.empty([dim, dim])

        from multiprocessing import Pool

        params = list(itertools.product(range(dim), range(dim)))

        with Pool(ncore) as p:
            hamil = p.starmap(self.calc_ham_entry, params)
            hamil = np.reshape(hamil, (dim, dim))
            hamil = np.triu(hamil)
            hamil = hamil + hamil.T - np.diag(np.diag(hamil))

        return hamil

    def diagonalize_H(self, hamil, target=0):

        vals, vecs = np.linalg.eigh(hamil)

        sorting = vals.argsort()

        return vals[sorting[target]], vecs[:, sorting[target]]

    def davidson(self, hamil, neigen=1, maxiter=100, tolerance=1e-8):

        # Get the dimenson of the hamiltonian
        dim = hamil.shape[0]
        # Identity matrix of same dimension
        identity = np.eye(dim)

        # Array holding trial eigenvectors
        V = np.eye(dim, 2 * neigen)

        # Array containing the norms
        norm = np.zeros(2 * neigen)

        for idx1 in range(maxiter):

            # Orthonormalise trial vectors via QR decomposition
            V, R = np.linalg.qr(V)

            # Compute matrix-vector producte sigma = Hv
            # and setup subspace matrix T = v^Tsigma
            h = np.dot(V.conj().T, np.dot(hamil, V))

            # solve eigenvalue problem in subspace
            vals, vecs = np.linalg.eig(h)

            # Sort eigevalue in subspace
            idcs = np.argsort(vals.real)
            vals = vals[idcs]
            vecs = vecs[:, idcs]

            # Compute Ritz vectors to approximate
            q = np.dot(V, vecs)

            for idx2 in range(2 * neigen):

                j = idx2 - int(0.5 * neigen)

                # get residual
                res = np.dot((hamil - vals[j] * identity), q[:, j])
                norm[idx2] = np.linalg.norm(res)

                if np.all(norm < tolerance):
                    break

                # preconditioning (+1e12 to avoid division by 0)
                delta = res / (vals[j] - np.diag(hamil) + 1e-12)
                delta /= np.linalg.norm(delta)

                V = np.hstack((V, delta.reshape(-1, 1)))

        return vals[:neigen], q[:, :neigen]

    def get_prop_vecs(self):

        prop_vecs = []

        assert sum(self.GAS_sizes) == self.nspatorbs

        for idx1 in range(len(self.occ_vecs)):

            vec = []

            for idx2 in range(len(self.GAS_sizes)):
                vec.append(
                    int(
                        len(
                            np.where(
                                self.occ_vecs[idx1]
                                <= np.sum(self.GAS_sizes[: idx2 + 1]) * 2
                            )[0]
                        )
                        - np.sum(vec)
                    )
                )

            prop_vecs.append(tuple(vec))

        return prop_vecs

    def get_contained_in_P(self):

        assert len(self.GAS_min) == len(self.GAS_max)

        contained_in_P = np.array([])

        for idx1 in range(len(self.prop_vecs)):
            status = True
            for idx2 in range(len(self.GAS_min)):
                if (
                    self.prop_vecs[idx1][idx2] < self.GAS_min[idx2]
                    or self.prop_vecs[idx1][idx2] > self.GAS_max[idx2]
                ):
                    status = False
            contained_in_P = np.append(contained_in_P, status)

        return contained_in_P

    def get_ordered_vecs(self):
        P_space = []
        Q_space = []

        for idx1 in range(len(self.prop_vecs)):
            status = True
            for idx2 in range(len(self.GAS_min)):
                if (
                    self.prop_vecs[idx1][idx2] < self.GAS_min[idx2]
                    or self.prop_vecs[idx1][idx2] > self.GAS_max[idx2]
                ):
                    status = False

            if status:
                P_space.append(self.occ_vecs[idx1])
            else:
                Q_space.append(self.occ_vecs[idx1])

        if len(Q_space) != 0:
            ordered_vecs = np.concatenate((P_space, Q_space))
        else:
            ordered_vecs = P_space

        self.P_space = P_space
        self.Q_space = Q_space

        return np.array(ordered_vecs, dtype=int)

    def build_loewdin(self, GAS_sizes, GAS_min, GAS_max, ncore=1):

        if self.basis == "guga":
            print("Use build_guga_loewdin")
            sys.exit()

        self.GAS_sizes = GAS_sizes
        self.GAS_min = GAS_min
        self.GAS_max = GAS_max
        self.prop_vecs = self.get_prop_vecs()

        """
        If we want
        PP PQ
        QP QQ
        Block structure in Hamiltonian
        """
        self.occ_vecs = self.get_ordered_vecs()
        self.prop_vecs = self.get_prop_vecs()

        self.contained_in_P = self.get_contained_in_P()

        dim = len(self.occ_vecs)
        hamil = np.empty([dim, dim])

        from multiprocessing import Pool

        params = list(itertools.product(range(dim), range(dim)))

        with Pool(ncore) as p:
            hamil = p.starmap(self.calc_loewdin_entry, params)
            hamil = np.reshape(hamil, (dim, dim))
            hamil = np.triu(hamil)
            hamil = hamil + hamil.T - np.diag(np.diag(hamil))

        return hamil

    def calc_loewdin_entry(self, i, j):

        if i > j:
            return 0.0

        vec1 = self.occ_vecs[i]
        vec2 = self.occ_vecs[j]

        diff = self.differ_by(vec1, vec2)

        if diff > 2:
            return 0.0
        elif diff == 2:
            if self.contained_in_P[i] or self.contained_in_P[j]:
                diff_idcs_1, diff_idcs_2 = self.differ_where(vec1, vec2)
                return self.hamil_diff_two(vec1, vec2, diff_idcs_1, diff_idcs_2)
            else:
                return 0.0
        elif diff == 1:
            if self.contained_in_P[i] or self.contained_in_P[j]:
                diff_idx_1, diff_idx_2 = self.differ_where(vec1, vec2)
                return self.hamil_diff_one(vec1, vec2, diff_idx_1, diff_idx_2)
            else:
                return 0.0
        elif diff == 0 and (vec1 == vec2).all():
            return self.hamil_diag(vec1) + self.fcidump["nuc"]

    def get_csfs(self):
        """
        A recursive function that returns a list of CSFs in step vector representation.
        S is S_total * 2, thus S must be an integer.
        i.e., S = 0 is a singlet, S = 1 is a doublet, S = 3 is a triplet, ...
        0: couple an empty orbital
        1: couple an orbital with up-spin
        2: couple an orbital with down-spin
        3: couple a doubly occupied orbital

        Author: Maru Song (https://github.com/MaruSongMaru)
        """

        a = int((self.nelecs - self.Stotal * 2) / 2)
        b = int(self.Stotal * 2)
        c = int(self.nspatorbs - a - b)

        if (
            a < 0
            or b < 0
            or c < 0
            or not (isinstance(a, int) and isinstance(b, int) and isinstance(c, int))
        ):
            raise ValueError("a, b, and c should be non-negative integers")

        def CSFgenerator(a, b, c, csf):

            if a == b == c == 0:

                self.csfs.append(csf[::-1])

            if a > 0:
                a_tmp = a - 1
                csf_tmp = csf.copy()
                csf_tmp.append(3)
                CSFgenerator(a_tmp, b, c, csf_tmp)

            if a > 0 and c > 0:
                a_tmp = a - 1
                b_tmp = b + 1
                c_tmp = c - 1
                csf_tmp = csf.copy()
                csf_tmp.append(2)
                CSFgenerator(a_tmp, b_tmp, c_tmp, csf_tmp)

            if b > 0:
                b_tmp = b - 1
                csf_tmp = csf.copy()
                csf_tmp.append(1)
                CSFgenerator(a, b_tmp, c, csf_tmp)

            if c > 0:
                c_tmp = c - 1
                csf_tmp = csf.copy()
                csf_tmp.append(0)
                CSFgenerator(a, b, c_tmp, csf_tmp)

        CSFgenerator(a, b, c, self.csf)

    def stepvec_to_definedet(self, csf):
        """
        Convert a CSF in step-vector representation to
        neci definedet representation

        Author: Maru Song (https://github.com/MaruSongMaru)
        """
        definedet = []
        for i in range(len(csf)):
            if csf[i] == 0:
                pass
            elif csf[i] == 1:
                definedet.append((i + 1) * 2 - 1)
            elif csf[i] == 2:
                definedet.append((i + 1) * 2)
            elif csf[i] == 3:
                definedet.append((i + 1) * 2 - 1)
                definedet.append((i + 1) * 2)
            else:
                raise ValueError(
                    "A step-vector element should be 0, 1, 2, or, 3. Current value is {}.".format(
                        csf[i]
                    )
                )
        return definedet

    def calc_guga_ham_entry(self, i, j):

        import sys

        sys.path.append(self.necipath)

        import neci_guga

        Di = self.stepvec_to_definedet(self.csfs[i])
        Dj = self.stepvec_to_definedet(self.csfs[j])

        if i > j:
            return 0
        else:
            return neci_guga.csf_matel(Di, Dj)

    def build_guga_hamil(self, necipath, ncore=1):

        self.necipath = necipath

        import sys

        sys.path.append(self.necipath)

        import neci_guga
        import itertools
        from multiprocessing import Pool

        neci_guga.init_guga(self.fcipath, self.Stotal, self.nelecs, self.nspatorbs)

        num_csfs = len(self.csfs)

        params = list(itertools.product(range(num_csfs), range(num_csfs)))

        with Pool(ncore) as p:

            hamil = p.starmap(self.calc_guga_ham_entry, params)
            hamil = np.reshape(hamil, (num_csfs, num_csfs))
            hamil = np.triu(hamil)
            hamil = hamil + hamil.T - np.diag(np.diag(hamil))

        neci_guga.clear_guga()

        return hamil

    def get_guga_prop_vecs_occ(self):

        prop_vecs = []

        assert sum(self.GAS_sizes) == self.nspatorbs

        for idx1 in range(len(self.csfs)):

            vec = []

            for idx2 in range(len(self.GAS_sizes)):
                stop = np.sum(self.GAS_sizes[: idx2 + 1])
                val = self.csfs_count_particles(self.csfs[idx1][:stop])
                vec.append(int(val - np.sum(vec)))

            prop_vecs.append(tuple(vec))

        return prop_vecs

    def csfs_count_particles(self, csf):

        count = 0

        for idx in range(len(csf)):
            if csf[idx] == 1 or csf[idx] == 2:
                count += 1
            elif csf[idx] == 3:
                count += 2

        return count

    def get_ordered_csfs(self):
        P_space = []
        Q_space = []

        for idx1 in range(len(self.prop_vecs)):
            status = True
            for idx2 in range(len(self.GAS_min)):
                if (
                    self.prop_vecs[idx1][idx2] < self.GAS_min[idx2]
                    or self.prop_vecs[idx1][idx2] > self.GAS_max[idx2]
                ):
                    status = False

            if status:
                P_space.append(self.csfs[idx1])
            else:
                Q_space.append(self.csfs[idx1])

        if len(Q_space) != 0:
            ordered_vecs = np.concatenate((P_space, Q_space))
        else:
            ordered_vecs = P_space

        self.P_space = P_space
        self.Q_space = Q_space

        return np.array(ordered_vecs, dtype=int)

    def build_guga_loewdin(
        self, GAS_sizes, GAS_min, GAS_max, necipath, mode="occ", ncore=1
    ):

        self.necipath = necipath

        import sys

        sys.path.append(self.necipath)

        import neci_guga
        import itertools

        neci_guga.init_guga(self.fcipath, self.Stotal, self.nelecs, self.nspatorbs)

        self.GAS_sizes = GAS_sizes
        self.GAS_min = GAS_min
        self.GAS_max = GAS_max
        if mode == "occ":
            self.prop_vecs = self.get_guga_prop_vecs_occ()
        else:
            print("Currently only occ numbers are supported in prop vec")
            sys.exit()

        """
        If we want
        PP PQ
        QP QQ
        Block structure in Hamiltonian
        """
        self.csfs = self.get_ordered_csfs()
        self.prop_vecs = self.get_guga_prop_vecs_occ()

        self.contained_in_P = self.get_contained_in_P()

        dim = len(self.csfs)
        hamil = np.empty([dim, dim])

        from multiprocessing import Pool

        params = list(itertools.product(range(dim), range(dim)))

        with Pool(ncore) as p:
            hamil = p.starmap(self.calc_guga_loewdin_entry, params)
            hamil = np.reshape(hamil, (dim, dim))
            hamil = np.triu(hamil)
            hamil = hamil + hamil.T - np.diag(np.diag(hamil))

        neci_guga.clear_guga()

        return hamil

    def calc_guga_loewdin_entry(self, i, j):

        import sys

        sys.path.append(self.necipath)

        import neci_guga

        Di = self.stepvec_to_definedet(self.csfs[i])
        Dj = self.stepvec_to_definedet(self.csfs[j])

        # lower triag always zero
        if i > j:
            return 0.0
        # diagonal always value
        elif i == j:
            return neci_guga.csf_matel(Di, Dj)
        else:
            if self.contained_in_P[i] or self.contained_in_P[j]:
                return neci_guga.csf_matel(Di, Dj)
            else:
                return 0.0
